Learning reStructuredText
===========================

The whole guide is documented using reStructuredText - a markup language for documentation.

Getting Started
----------------

Here are some of the resources to get you Started.

* `Official Documentaion from Sphinx <https://sphinx-tutorial.readthedocs.io/start/>`_
* `Sphinx Tutorial <https://sphinx-tutorial.readthedocs.io/step-1/>`_
  