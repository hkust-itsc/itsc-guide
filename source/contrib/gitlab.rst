Gitlab Repository
==================

The guide is hosted in GitLab. You may find the repository `here <https://gitlab.com/hkust-itsc/itsc-guide>`_.

Contributing to the Repository
-------------------------------

You may contribute to the repository no matter you are added to the repository as a developer or not. You may simply open a merge request to request changes.