Welcome to ITSC Intern Guide!
=============================================

This guide contains all you need to know for our ITSC duties.

.. toctree::
   :maxdepth: 1
   :caption: Getting Started
   :hidden:

.. toctree::
   :maxdepth: 1
   :caption: Intern Management System
   :hidden:

.. toctree::
   :maxdepth: 1
   :caption: General Duties
   :hidden:

.. toctree::
   :maxdepth: 1
   :caption: Barn Specific Duties
   :hidden:

.. toctree::
   :maxdepth: 1
   :caption: LSK Specific Duties
   :hidden:

.. toctree::
   :maxdepth: 1
   :caption: Service Desk Specific Duties
   :hidden:

.. toctree::
   :maxdepth: 1
   :caption: Contacts
   :hidden:

.. toctree::
   :maxdepth: 1
   :caption: Contribute
   :hidden:

   contrib/contrib
   contrib/rst
   contrib/gitlab

.. toctree::
   :maxdepth: 1
   :caption: Appendix
   :hidden:

   appendix/cat
